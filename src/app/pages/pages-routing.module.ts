import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodoListComponent } from '../features/components/todo-list/todo-list.component';
import { TodoFormComponent } from '../features/components/todo-form/todo-form.component';


const routes: Routes = [
  { path: 'todos', component: TodoListComponent},
  { path: 'new-todo', component: TodoFormComponent},
  { path: '', pathMatch: 'full', redirectTo: '/home/todos' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
