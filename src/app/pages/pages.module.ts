import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/home/home.component';
import { SharedModule } from '../shared/shared.module';
import { LoginComponent } from './components/login/login.component';
import { FeaturesModule } from '../features/features.module';
import { PagesRoutingModule } from './pages-routing.module';



@NgModule({
  declarations: [HomeComponent, LoginComponent],
  imports: [
    CommonModule,
    SharedModule,
    FeaturesModule,
    PagesRoutingModule
  ],
  exports: [HomeComponent, LoginComponent]
})
export class PagesModule { }
