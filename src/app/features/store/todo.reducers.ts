import { Action, createReducer, on } from '@ngrx/store';
import * as TodosActions from './todo.actions';

export interface TodosState {
  data: any;
  pending: boolean;
  error: boolean;
  isFetchCompleted: boolean;
}

export const initialState: TodosState = {
  data: null,
  pending: null,
  error: null,
  isFetchCompleted: null,
};

const todosReducer = createReducer(
    initialState,
    on(TodosActions.FetchPending, state => ({ ...state, pending: true })),
    on(TodosActions.FetchFulfilled, (state, { todos }) => {
        return { ...state, data: todos, pending: false, isFetchCompleted: true }
    }
    ),
    on(TodosActions.FetchError, state => ({ ...state, error: true, pending: false })),
    on(TodosActions.ClearData, state => (initialState))
  );
  
export function reducer(state: TodosState | undefined, action: Action) {
    return todosReducer(state, action);
}
