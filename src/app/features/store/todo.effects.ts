import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { TodosService } from '../services/todos.service';
 
@Injectable()
export class TodosEffects {
 
  loadTodos$ = createEffect(() =>
    this.actions$.pipe(
      ofType('[TODOS ACTIONS: PENDING]'),
      mergeMap(() => this.todosService.getAllTodos()
        .pipe(
          map(todos => {
            //   console.log('effect', todos);
              return { type: '[TODOS ACTIONS: FULFILLED]', todos }
          }),
          catchError(() => of({ type: '[TODOS ACTIONS: ERROR]' }))
        )
      )
    )
  );
 
  constructor(
    private actions$: Actions,
    private todosService: TodosService
  ) {}
}