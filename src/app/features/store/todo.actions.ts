// import { Action } from '@ngrx/store';
import { createAction, props } from '@ngrx/store';
import { Todo } from '../models/todo.model';

export enum TodosActionsTypes {
  FETCH_PENDING = '[TODOS ACTIONS: PENDING]',
  FETCH_FULFILLED = '[TODOS ACTIONS: FULFILLED]',
  FETCH_ERROR = '[TODOS ACTIONS: ERROR]',
  CLEAR_DATA = '[TODOS ACTIONS] CLEAR DATA',
}

export const FetchPending = createAction(
    '[TODOS ACTIONS: PENDING]'
);

export const FetchFulfilled = createAction(
    '[TODOS ACTIONS: FULFILLED]',
    props<{ todos: Todo[] }>()
);

export const FetchError = createAction(
    '[TODOS ACTIONS: ERROR]',
    props<{ error: string }>()
);

export const ClearData = createAction(
    '[TODOS ACTIONS: CLEAR DATA]'
);

