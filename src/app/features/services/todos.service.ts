import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class TodosService {

  private endpoint = `${environment.BASE_URL_API}/todos`;

  constructor(
    private http: HttpClient
  ) {}

  getAllTodos() {
    return this.http.get(this.endpoint);
  }

  getTodoById(id: number) {
    const url = `${this.endpoint}/${id}`;
  }
}
