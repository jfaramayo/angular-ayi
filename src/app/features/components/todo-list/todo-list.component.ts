import { Component, OnInit, OnDestroy } from '@angular/core';
import { Todo } from '../../models/todo.model';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';

import * as fromActionsTodos from 'src/app/features/store/todo.actions';
import { TodosState } from 'src/app/features/store/todo.reducers';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit, OnDestroy {

  public todoList: Todo[] = [];
  public isLoading = true;
  private subscriptions: Subscription[] = [];

  constructor(
    private store: Store<{ todos: TodosState }>,
  ) { }

  ngOnInit(): void {
    this.store.dispatch(fromActionsTodos.FetchPending());
    this.store.subscribe( ({ todos }) => {
      this.isLoading = todos.pending;
      if(todos.data && todos.data.length) {

        this.formatTodos(todos.data);
      }
    })
  }

  formatTodos(todos: any) {
    this.todoList = todos.map(todo => {
      return {
        id: todo.id,
        title: todo.title,
        description: 'This is description',
        completed: todo.completed
      }
    });
  }

  renderTodos() {
    this.isLoading = false;
  }

  ngOnDestroy() {
    this.subscriptions.forEach( (s: Subscription) => s.unsubscribe() );
  }

}
